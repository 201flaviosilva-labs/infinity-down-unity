﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlatfomScript : MonoBehaviour
{

    public float moveSpeed = 2f;
    public float boundY = 6f;

    public bool movingPlatformLeft, movingPlatformRight, isBreakble, isSpike, isPlatform;

    private Animator anim;

    void Awake()
    {
        if (isBreakble)
        {
            anim = GetComponent<Animator>();
        }
    }

    // Update is called once per frame
    void Update()
    {
        Move();
    }

    void Move()
    {
        Vector2 temp = transform.position;
        temp.y += moveSpeed * Time.deltaTime;
        transform.position = temp;

        if (temp.y >= boundY)
        {
            gameObject.SetActive(false);
        }
    }

    void BreakbleDeactivate()
    {
        Invoke("DeactivateGameObject", 0.3f);
    }

    void DeactivateGameObject()
    {
        gameObject.SetActive(false);
    }

    void OnCollisionEnter2D(Collision2D traget)
    {
        if (traget.gameObject.tag == "Player")
        {
            if (isBreakble)
            {
                anim.Play("Break");
            }

            if (isSpike)
            {
                traget.transform.position = new Vector2(1000f, 1000f);
                SceneManager.LoadScene("GamePlay");
            }
        }
    }

    void OnCollisionStay2D(Collision2D traget)
    {
        if (traget.gameObject.tag == "Player")
        {
            if (movingPlatformLeft)
            {
                traget.gameObject.GetComponent<PlayerMovement>().PlatformMove(-1f);
            }

            if (movingPlatformRight)
            {
                traget.gameObject.GetComponent<PlayerMovement>().PlatformMove(1f);
            }
        }
    }
}
