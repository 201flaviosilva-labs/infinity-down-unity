﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerBounds : MonoBehaviour
{

    public float minX = -2.6f, maxX = 2.6f, minY = -5.6f;

    private bool outOfBounds;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        CheckBounds();
    }

    void CheckBounds()
    {
        Vector2 temp = transform.position;

        if (temp.x > maxX) temp.x = maxX;
        if (temp.x < minX) temp.x = minX;

        transform.position = temp;

        if (temp.y <= minY)
        {
            if (!outOfBounds)
            {
                outOfBounds = true;
                SceneManager.LoadScene("GamePlay");
            }
        }

    }

    void OnTriggerEnter2D(Collider2D target)
    {
        if (target.tag == "TopSpike")
        {
            // transform.position = new Vector2(1000f, 1000f);
            SceneManager.LoadScene("GamePlay");
        }
    }
}
